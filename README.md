<div align="center">
<a href="https://victorhck.gitlab.io/privacytools-es/">
	<img src="assets/img/svg/layout/brand/horizontal.svg" width="500px" alt="PrivacyTools" />
</a>
</div>

_Cifrado contra el seguimiento global_

# ¿Qué es esta página?

Este proyecto nace como interés personal de traducir al castellano la página original de [privacytools.io](https://www.privacytools.io/) alojada en GitHub. La página original ofrece una importante colección de recursos e información contra el seguimiento masivo de nuestros datos en la actual sociedad tecnológica.

Puedes ver la página en este enlace:
* [https://victorhck.gitlab.io/privacytools-es/](https://victorhck.gitlab.io/privacytools-es/)

Esta página está creada como un "fork" de la página en inglés antes mencionada, con ella trato de ofrecer a la comunidad de habla catellana información sobre herramientas para proteger su privacidad. Trataré de mantenerla actualizada a los últimos cambios que se realicen en la página original. Está mantenida y traducida sólo por [mi](http://victorhckinthefreeworld.com/). Míos por tanto serán los aciertos y errores. Para subsanar los segundos, manda mejoras o sugerencias.

Todo el trabajo de traducción y mantenimiento está hecho de manera desinteresada, con el único fin de dar a conocer las distintas herramientas y como medio de conocimiento y aprendizaje.

El _feedback_ tanto positivo como negativo (siempre que sea constructivo y respetuoso) siempre es bien recibido. Si quieres invitarme a una cerveza por tantas horas de traducción, picado de texto y demás puedes hacerlo mediante [LiberaPay](https://es.liberapay.com/victorhck/donate)


# Licencia
CC0 1.0 Universal
